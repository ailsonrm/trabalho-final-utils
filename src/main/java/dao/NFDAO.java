package dao;

import entity.NF;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.faces.bean.ManagedBean;
import java.beans.Transient;
import java.io.Serializable;
import java.util.List;

@ManagedBean

public class NFDAO implements Serializable{

    Session session = null;
    Transaction transaction = null;

    public List<NF> getALLNFs() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        transaction = session.beginTransaction();
        Query query = session.createQuery("from NF");
        return query.list();
    }
}
