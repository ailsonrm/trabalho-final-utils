package bean;
 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 
import org.primefaces.event.ToggleEvent;
 
@ManagedBean
public class FieldsetView {

    String infoText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\" name=\"NF_Info\" pageWidth=\"595\" pageHeight=\"842\" whenNoDataType=\"NoDataSection\" columnWidth=\"555\" leftMargin=\"20\" rightMargin=\"20\" topMargin=\"20\" bottomMargin=\"20\" isIgnorePagination=\"true\" uuid=\"f0059bf4-9c9c-4fac-a44d-12db94885639\">\n" +
            "\t<property name=\"com.jaspersoft.studio.data.defaultdataadapter\" value=\"MySQL - (trab_final_utils)\"/>\n" +
            "\t<property name=\"ireport.zoom\" value=\"1.0000000000000056\"/>\n" +
            "\t<property name=\"ireport.x\" value=\"0\"/>\n" +
            "\t<property name=\"ireport.y\" value=\"0\"/>\n" +
            "\t<parameter name=\"NF_ID\" class=\"java.math.BigDecimal\"/>\n" +
            "\t<parameter name=\"COM_BOLETO\" class=\"java.lang.Boolean\">\n" +
            "\t\t<defaultValueExpression><![CDATA[false]]></defaultValueExpression>\n" +
            "\t</parameter>\n" +
            "\t<parameter name=\"SUBREPORT_DIR\" class=\"java.lang.String\" isForPrompting=\"false\">\n" +
            "\t\t<defaultValueExpression><![CDATA[\"\\\\\"]]></defaultValueExpression>\n" +
            "\t</parameter>\n" +
            "\t<queryString>\n" +
            "\t\t<![CDATA[SELECT * FROM NF WHERE NF_ID = $P{NF_ID}]]>\n" +
            "\t</queryString>\n" +
            "\t<field name=\"NF_ID\" class=\"java.lang.Integer\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"NAME\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"ADDRESS\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"CITY\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"UF\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"CNPJ\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"BANK_NAME\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"EXPIRE_DATE\" class=\"java.sql.Timestamp\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"DOC_DATE\" class=\"java.sql.Timestamp\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"OBSERVATION\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"DOC_AMOUNT\" class=\"java.math.BigDecimal\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"FINE_AMOUNT\" class=\"java.math.BigDecimal\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"PAY_VALUE\" class=\"java.math.BigDecimal\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"BILET\" class=\"java.lang.Boolean\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<title>\n" +
            "\t\t<band height=\"65\" splitType=\"Stretch\">\n" +
            "\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t<property name=\"com.jaspersoft.studio.layout\" value=\"com.jaspersoft.studio.editor.layout.HorizontalRowLayout\"/>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"bffc948a-772d-4ab5-9b2a-e942ab9b9f15\" key=\"\" mode=\"Transparent\" x=\"0\" y=\"0\" width=\"555\" height=\"65\" forecolor=\"#F01C18\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.x\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\" markup=\"html\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" size=\"26\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Pagamento de Títulos]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t</band>\n" +
            "\t</title>\n" +
            "\t<pageHeader>\n" +
            "\t\t<band height=\"122\" splitType=\"Stretch\">\n" +
            "\t\t\t<rectangle>\n" +
            "\t\t\t\t<reportElement uuid=\"efab1a64-6d6e-4cd7-9e2a-994aee08ec94\" x=\"0\" y=\"10\" width=\"555\" height=\"75\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<graphicElement>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</graphicElement>\n" +
            "\t\t\t</rectangle>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"6a622cdc-7267-469a-a275-daf0de770e2e\" key=\"staticText-3\" x=\"0\" y=\"29\" width=\"60\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Nome: ]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"4d305952-6cf5-496b-986a-b25662a530b6\" key=\"staticText-4\" x=\"0\" y=\"43\" width=\"60\" height=\"14\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Endereço:]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"f3e516f6-0821-400b-9890-bfd352cd0cb7\" key=\"staticText-5\" x=\"0\" y=\"57\" width=\"60\" height=\"14\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Cidade/UF:]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"a4c935f9-933e-442f-9316-3a58fd05d475\" key=\"staticText-6\" x=\"0\" y=\"71\" width=\"60\" height=\"14\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[CNPJ:]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"08c3d402-c8fe-4af9-ad46-d682bd0f2290\" x=\"60\" y=\"29\" width=\"495\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{NAME}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"62ad8955-a1fa-415a-824e-fd660596e520\" x=\"60\" y=\"43\" width=\"495\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{ADDRESS}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"541f750c-d9d5-4f10-8e3c-fb829250c339\" x=\"60\" y=\"57\" width=\"100\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{CITY}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField pattern=\"###0.00\">\n" +
            "\t\t\t\t<reportElement uuid=\"be9b5d31-89ad-450f-94fb-30c39d489b1f\" x=\"60\" y=\"71\" width=\"200\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{CNPJ}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"7b953a3b-d557-4bdf-8cbc-479320f5e0f7\" x=\"160\" y=\"57\" width=\"100\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{UF}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"3c0c7c67-ab1a-4a43-9251-389348be15cf\" x=\"0\" y=\"10\" width=\"555\" height=\"14\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement>\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" size=\"11\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ - Dados do Cliente]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"15211915-0edc-4beb-9d7b-fe9d6fec1a1d\" key=\"staticText-2\" x=\"0\" y=\"85\" width=\"100\" height=\"10\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Data]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"db4f0231-acfc-49d8-9053-5afc9715ee73\" x=\"0\" y=\"95\" width=\"555\" height=\"14\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement>\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" size=\"11\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ - Dados Nota Fiscal]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"8443bbb4-04b4-4420-b1ef-cfbe35284468\" key=\"staticText-2\" x=\"0\" y=\"109\" width=\"100\" height=\"13\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Número]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"43239caa-1cfb-424f-befc-f858c5865e83\" key=\"staticText-2\" x=\"100\" y=\"109\" width=\"255\" height=\"13\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Descrição]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"89208952-ce18-4638-8a06-7a10c164d451\" key=\"staticText-2\" x=\"355\" y=\"109\" width=\"100\" height=\"13\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Data]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"050ff51a-1f8a-429f-b792-3644435c89fb\" key=\"staticText-2\" x=\"455\" y=\"109\" width=\"100\" height=\"13\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Valor]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t</band>\n" +
            "\t</pageHeader>\n" +
            "\t<detail>\n" +
            "\t\t<band height=\"25\" splitType=\"Stretch\">\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"df022eb4-270b-48b1-8a13-6ae8e29e710a\" x=\"0\" y=\"0\" width=\"100\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\"/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{NF_ID}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"d7795616-4e1a-4ae4-8b03-919f7364764a\" x=\"100\" y=\"0\" width=\"255\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{OBSERVATION}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField pattern=\"dd/MM/yyyy\">\n" +
            "\t\t\t\t<reportElement uuid=\"36faba02-ee49-4676-8ffb-b762fc2f3e09\" x=\"355\" y=\"0\" width=\"100\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\"/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{EXPIRE_DATE}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField pattern=\"¤ #,##0.00\">\n" +
            "\t\t\t\t<reportElement uuid=\"7b813582-6d9e-4a51-8d9f-7a42c0ca72b9\" x=\"455\" y=\"0\" width=\"100\" height=\"14\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.width\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.5\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.0\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\"/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{PAY_VALUE}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<line>\n" +
            "\t\t\t\t<reportElement uuid=\"64b7c931-df47-4b78-9cd6-4a695eea7e32\" x=\"0\" y=\"14\" width=\"555\" height=\"1\"/>\n" +
            "\t\t\t\t<graphicElement>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</graphicElement>\n" +
            "\t\t\t</line>\n" +
            "\t\t</band>\n" +
            "\t</detail>\n" +
            "\t<columnFooter>\n" +
            "\t\t<band height=\"334\" splitType=\"Immediate\">\n" +
            "\t\t\t<printWhenExpression><![CDATA[$P{COM_BOLETO}]]></printWhenExpression>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"4c9df45a-4887-41ed-90a9-7a0aa6297559\" x=\"0\" y=\"0\" width=\"555\" height=\"334\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement>\n" +
            "\t\t\t\t\t<font fontName=\"SansSerif\" size=\"11\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ - Boleto]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<rectangle>\n" +
            "\t\t\t\t<reportElement uuid=\"9947b7b7-16af-48bd-81a7-486904998246\" x=\"0\" y=\"14\" width=\"555\" height=\"320\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<graphicElement>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.5\" lineStyle=\"Dashed\" lineColor=\"#999999\"/>\n" +
            "\t\t\t\t</graphicElement>\n" +
            "\t\t\t</rectangle>\n" +
            "\t\t\t<subreport>\n" +
            "\t\t\t\t<reportElement uuid=\"674cf9b5-2b78-417c-add6-dd2f7c0b60ff\" x=\"0\" y=\"14\" width=\"555\" height=\"320\">\n" +
            "\t\t\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t\t</reportElement>\n" +
            "\t\t\t\t<parametersMapExpression><![CDATA[$P{REPORT_PARAMETERS_MAP}]]></parametersMapExpression>\n" +
            "\t\t\t\t<subreportParameter name=\"SUBREPORT_DIR\">\n" +
            "\t\t\t\t\t<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>\n" +
            "\t\t\t\t</subreportParameter>\n" +
            "\t\t\t\t<subreportParameter name=\"NF_ID\">\n" +
            "\t\t\t\t\t<subreportParameterExpression><![CDATA[$P{NF_ID}]]></subreportParameterExpression>\n" +
            "\t\t\t\t</subreportParameter>\n" +
            "\t\t\t\t<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>\n" +
            "\t\t\t\t<subreportExpression><![CDATA[$P{SUBREPORT_DIR}+\"NF_Boleto.jasper\"]]></subreportExpression>\n" +
            "\t\t\t</subreport>\n" +
            "\t\t</band>\n" +
            "\t</columnFooter>\n" +
            "\t<pageFooter>\n" +
            "\t\t<band height=\"15\">\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"6ecba504-23f9-412b-9f90-c624b6ce1906\" x=\"435\" y=\"0\" width=\"80\" height=\"15\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\"/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[\"Page \"+$V{PAGE_NUMBER}+\" of\"]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField evaluationTime=\"Report\">\n" +
            "\t\t\t\t<reportElement uuid=\"a2d2f24d-6c51-4b10-b1bf-f5342195db0a\" x=\"515\" y=\"0\" width=\"40\" height=\"15\"/>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[\" \" + $V{PAGE_NUMBER}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField pattern=\"EEEEE dd MMMMM yyyy\">\n" +
            "\t\t\t\t<reportElement uuid=\"0bec92c1-43dd-4ba7-b899-5e684b4bdf0e\" x=\"0\" y=\"0\" width=\"150\" height=\"15\"/>\n" +
            "\t\t\t\t<textElement/>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t</band>\n" +
            "\t</pageFooter>\n" +
            "\t<noData>\n" +
            "\t\t<band height=\"40\">\n" +
            "\t\t\t<property name=\"com.jaspersoft.studio.unit.height\" value=\"pixel\"/>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"de25bb5c-6a34-4c30-8bcb-d9be4753f3c2\" x=\"0\" y=\"0\" width=\"555\" height=\"40\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font size=\"30\" isBold=\"true\" isUnderline=\"false\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Não contém informações!!!]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t</band>\n" +
            "\t</noData>\n" +
            "</jasperReport>\n";

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    public String getBoletoText() {
        return boletoText;
    }

    public void setBoletoText(String boletoText) {
        this.boletoText = boletoText;
    }

    String boletoText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<jasperReport xmlns=\"http://jasperreports.sourceforge.net/jasperreports\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd\" name=\"NF_Boleto\" pageWidth=\"555\" pageHeight=\"800\" columnWidth=\"545\" leftMargin=\"5\" rightMargin=\"5\" topMargin=\"5\" bottomMargin=\"5\" uuid=\"e9aa4544-8099-48a1-8bbe-595370d7221e\">\n" +
            "\t<property name=\"com.jaspersoft.studio.data.defaultdataadapter\" value=\"MySQL - (trab_final_utils)\"/>\n" +
            "\t<property name=\"ireport.zoom\" value=\"1.3310000000000073\"/>\n" +
            "\t<property name=\"ireport.x\" value=\"0\"/>\n" +
            "\t<property name=\"ireport.y\" value=\"0\"/>\n" +
            "\t<parameter name=\"NF_ID\" class=\"java.math.BigDecimal\"/>\n" +
            "\t<queryString>\n" +
            "\t\t<![CDATA[SELECT * FROM NF WHERE NF_ID =  $P{NF_ID}]]>\n" +
            "\t</queryString>\n" +
            "\t<field name=\"NF_ID\" class=\"java.lang.Integer\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"NAME\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"ADDRESS\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"CITY\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"UF\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"CNPJ\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"BANK_NAME\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"EXPIRE_DATE\" class=\"java.sql.Timestamp\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"DOC_DATE\" class=\"java.sql.Timestamp\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"OBSERVATION\" class=\"java.lang.String\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"DOC_AMOUNT\" class=\"java.math.BigDecimal\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"FINE_AMOUNT\" class=\"java.math.BigDecimal\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<field name=\"PAY_VALUE\" class=\"java.math.BigDecimal\">\n" +
            "\t\t<fieldDescription><![CDATA[]]></fieldDescription>\n" +
            "\t</field>\n" +
            "\t<detail>\n" +
            "\t\t<band height=\"310\">\n" +
            "\t\t\t<textField isBlankWhenNull=\"false\">\n" +
            "\t\t\t\t<reportElement uuid=\"c28afb35-dc42-49f2-ac85-0eeccb2cafb1\" key=\"textField-12\" x=\"0\" y=\"0\" width=\"99\" height=\"15\" forecolor=\"#FF0000\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font size=\"11\" isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{BANK_NAME}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"8fe24eae-d75f-4dcb-b7bb-b5340ef4e7a6\" key=\"staticText-14\" x=\"389\" y=\"0\" width=\"156\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Número do Título]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"995c0d9c-ebb6-412d-94ab-91f14e5b74c9\" key=\"staticText-13\" x=\"278\" y=\"0\" width=\"111\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Nosso Número]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"ec7807ae-f946-42a5-8ebe-5f2a711e4d5e\" key=\"staticText-12\" x=\"99\" y=\"0\" width=\"179\" height=\"15\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\"/>\n" +
            "\t\t\t\t<text><![CDATA[Recibo do Sacado ]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<line>\n" +
            "\t\t\t\t<reportElement uuid=\"f6d8703b-f441-49df-a305-8a3618d8c1ec\" key=\"line-2\" x=\"0\" y=\"15\" width=\"545\" height=\"1\"/>\n" +
            "\t\t\t</line>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"05d4f583-ebc3-47b1-a169-1999c92ec5a1\" key=\"staticText-19\" x=\"278\" y=\"16\" width=\"111\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (+) Mora/Multa]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"1d822d0c-95a5-4e71-9125-f798a0fd4516\" key=\"staticText-20\" x=\"389\" y=\"16\" width=\"156\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (=) Valor Cobrado]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"4631922b-e15c-4834-aed0-4cc8c608d141\" key=\"staticText-15\" x=\"0\" y=\"16\" width=\"99\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Vencimento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"04211091-a3c5-4559-ab17-6f6ae01e0495\" key=\"staticText-18\" x=\"214\" y=\"16\" width=\"64\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Quantidade]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"10e42542-159b-48eb-8e56-774750e9cfaf\" key=\"staticText-17\" x=\"188\" y=\"16\" width=\"26\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Espécie]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"83adc7fd-aedf-44dc-8896-d1ce58c9ae2a\" key=\"staticText-16\" x=\"99\" y=\"16\" width=\"89\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Agência/Código Cedente]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<textField pattern=\"dd/MM/yyyy\">\n" +
            "\t\t\t\t<reportElement uuid=\"d0121645-f903-425d-a97b-8bf3a06326d6\" x=\"19\" y=\"19\" width=\"75\" height=\"12\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\" verticalAlignment=\"Bottom\">\n" +
            "\t\t\t\t\t<font size=\"8\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{EXPIRE_DATE}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"7ba0f0e0-c647-40cf-bc87-70ac6ae873fe\" key=\"staticText-49\" x=\"278\" y=\"39\" width=\"267\" height=\"37\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Center\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\" isStrikeThrough=\"false\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[Autenticação Mecânica]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"0465fba2-0df0-4427-88e3-b519529fe588\" key=\"staticText-24\" x=\"0\" y=\"61\" width=\"278\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Apólice/Documento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"02243769-4065-453c-97d9-378f9964fd86\" key=\"staticText-22\" x=\"150\" y=\"31\" width=\"128\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (-) Desconto/Abatimento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"e1d16150-43aa-4dd1-bfa7-539874294adf\" key=\"staticText-21\" x=\"0\" y=\"31\" width=\"150\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (=) Valor do Contrato]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"eb311749-04a5-4ba4-b2f0-22a828b31768\" key=\"staticText-23\" x=\"0\" y=\"46\" width=\"278\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Sacado]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<line>\n" +
            "\t\t\t\t<reportElement uuid=\"9c0c1c77-5bc6-4270-bbe2-37c89b32fa44\" key=\"line-1\" x=\"0\" y=\"96\" width=\"545\" height=\"1\"/>\n" +
            "\t\t\t</line>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"483b5884-a5c1-4e3a-a682-e21c483ebc5f\" key=\"staticText-26\" mode=\"Opaque\" x=\"389\" y=\"97\" width=\"156\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Vencimento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"a4321d0a-5904-4533-84b4-ff445691def1\" key=\"staticText-25\" x=\"0\" y=\"97\" width=\"389\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Local de Pagamento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"f37bf573-7ad4-47a6-95cf-15c76e4e5a07\" key=\"staticText-28\" mode=\"Transparent\" x=\"389\" y=\"112\" width=\"156\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Agência/Código Cedent e]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"3e35811c-f634-4647-b789-a8b4345637c2\" key=\"staticText-27\" x=\"0\" y=\"112\" width=\"389\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Cedente]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"0f1cc750-2c14-44fe-9817-a28054b534f3\" key=\"staticText-34\" mode=\"Opaque\" x=\"389\" y=\"127\" width=\"156\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Nosso Número]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"bc483b00-13ab-42d5-941e-6570541bb11a\" key=\"staticText-31\" x=\"214\" y=\"127\" width=\"50\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Especie Doc]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"3c8bd2df-46ba-49d9-9cab-62e549bf80ad\" key=\"staticText-30\" mode=\"Opaque\" x=\"106\" y=\"127\" width=\"108\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Número Documento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"4bda85de-6802-4e6d-8ccb-fcbdc4aa79ed\" key=\"staticText-32\" x=\"264\" y=\"127\" width=\"96\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Data Processamento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"52f7a34e-42ee-4d4d-ba1b-0d984981907b\" key=\"staticText-33\" x=\"360\" y=\"127\" width=\"29\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Aceite]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"a74c0379-6a64-437d-9499-3a844d57a7e6\" key=\"staticText-29\" x=\"0\" y=\"127\" width=\"106\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Data Documento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<textField pattern=\"dd/MM/yyyy\">\n" +
            "\t\t\t\t<reportElement uuid=\"81d41ee6-46c6-4416-8410-4f308863380a\" x=\"26\" y=\"130\" width=\"75\" height=\"12\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\" verticalAlignment=\"Bottom\">\n" +
            "\t\t\t\t\t<font size=\"8\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{DOC_DATE}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"e0415deb-068a-4eb4-99df-227fdddd1153\" key=\"staticText-36\" mode=\"Opaque\" x=\"139\" y=\"142\" width=\"49\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Carteira]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"dc522c27-81dc-46ac-9fcc-679d9f0acaef\" key=\"staticText-38\" mode=\"Opaque\" x=\"238\" y=\"142\" width=\"66\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Quantidade Moeda]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"c66ca0e9-287a-4905-87a0-83ccd25024b7\" key=\"staticText-35\" x=\"0\" y=\"142\" width=\"139\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Uso do Banco]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"cd574b83-9a61-494b-b360-b53c21ca9697\" key=\"staticText-37\" x=\"188\" y=\"142\" width=\"50\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Especie Moeda]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"0911539e-2e95-4255-9a01-960b0d90dfec\" key=\"staticText-40\" mode=\"Opaque\" x=\"389\" y=\"142\" width=\"156\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (=) Valor Documento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"ec9b1bdf-0633-46a4-9142-93383e790b94\" key=\"staticText-39\" x=\"304\" y=\"142\" width=\"85\" height=\"15\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Valor Moeda]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<textField pattern=\"¤ #,##0.00\">\n" +
            "\t\t\t\t<reportElement uuid=\"df3cd53b-2990-4197-857d-8698b5ae6f6f\" x=\"450\" y=\"145\" width=\"90\" height=\"12\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\" verticalAlignment=\"Bottom\">\n" +
            "\t\t\t\t\t<font size=\"8\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{DOC_AMOUNT}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"24685bb5-c5c7-4f97-9020-a31c3a758bbb\" key=\"staticText-43\" mode=\"Transparent\" x=\"389\" y=\"187\" width=\"156\" height=\"15\" backcolor=\"#FFFFFF\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (+) Mora/Multa]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"1b86b746-ed5b-4234-a184-bb2d965ca3f1\" key=\"staticText-44\" mode=\"Transparent\" x=\"389\" y=\"202\" width=\"156\" height=\"15\" backcolor=\"#FFFFFF\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (+) Outros Acréscimos]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"71483a11-9265-4ad4-aeb9-7973c77d581c\" key=\"staticText-41\" mode=\"Transparent\" x=\"389\" y=\"157\" width=\"156\" height=\"15\" backcolor=\"#FFFFFF\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (-) Desconto/Abatimento]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"3659537b-0512-4dcc-b8a8-d9147771a7fa\" key=\"staticText-46\" x=\"0\" y=\"157\" width=\"389\" height=\"75\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.25\" lineStyle=\"Solid\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Instruções]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"ceb31e44-86dd-493b-bf28-c04a7f69e12e\" key=\"staticText-45\" mode=\"Opaque\" x=\"389\" y=\"217\" width=\"156\" height=\"15\" backcolor=\"#CCCCCC\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (=) Valor Cobrado]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"24c820c1-029f-4014-8e3f-6dce92346ed6\" key=\"staticText-42\" mode=\"Transparent\" x=\"389\" y=\"172\" width=\"156\" height=\"15\" backcolor=\"#FFFFFF\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ (-) Outras Deduções]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"e9df40bd-d6dc-408f-872e-66591c051640\" x=\"6\" y=\"172\" width=\"380\" height=\"55\"/>\n" +
            "\t\t\t\t<textElement>\n" +
            "\t\t\t\t\t<font size=\"8\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{OBSERVATION}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField pattern=\"¤ #,##0.00\">\n" +
            "\t\t\t\t<reportElement uuid=\"706d59a8-2282-41e8-b793-526f8a546b11\" x=\"450\" y=\"190\" width=\"90\" height=\"12\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\" verticalAlignment=\"Bottom\">\n" +
            "\t\t\t\t\t<font size=\"8\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{FINE_AMOUNT}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<textField pattern=\"¤ #,##0.00\">\n" +
            "\t\t\t\t<reportElement uuid=\"eaf3e42b-3d74-4beb-8c51-fa4c313b47d0\" x=\"450\" y=\"220\" width=\"90\" height=\"12\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\" verticalAlignment=\"Bottom\">\n" +
            "\t\t\t\t\t<font size=\"8\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{PAY_VALUE}]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"745109c1-b026-42e2-ac4b-a71af290d1e5\" key=\"staticText-48\" x=\"0\" y=\"252\" width=\"545\" height=\"13\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Bottom\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Sacador / Avalista\t                     Código de Baixa]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<staticText>\n" +
            "\t\t\t\t<reportElement uuid=\"2f232f73-b445-40a2-9e6a-eb50d2d54ecd\" key=\"staticText-47\" x=\"0\" y=\"232\" width=\"545\" height=\"20\"/>\n" +
            "\t\t\t\t<box>\n" +
            "\t\t\t\t\t<pen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<topPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<leftPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<bottomPen lineWidth=\"0.25\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t\t<rightPen lineWidth=\"0.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>\n" +
            "\t\t\t\t</box>\n" +
            "\t\t\t\t<textElement textAlignment=\"Left\" verticalAlignment=\"Top\">\n" +
            "\t\t\t\t\t<font size=\"6\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<text><![CDATA[ Sacado]]></text>\n" +
            "\t\t\t</staticText>\n" +
            "\t\t\t<componentElement>\n" +
            "\t\t\t\t<reportElement uuid=\"9593f20d-1d98-4936-9b43-6066378f1a37\" x=\"0\" y=\"267\" width=\"264\" height=\"40\"/>\n" +
            "\t\t\t\t<jr:barbecue xmlns:jr=\"http://jasperreports.sourceforge.net/jasperreports/components\" xsi:schemaLocation=\"http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd\" type=\"3of9\" drawText=\"false\" checksumRequired=\"false\">\n" +
            "\t\t\t\t\t<jr:codeExpression><![CDATA[$F{NF_ID}+\".\"+\n" +
            "$F{CNPJ}+\".\"+\n" +
            "new SimpleDateFormat(\"ddMMyyyy\").format($F{DOC_DATE})+\n" +
            "new SimpleDateFormat(\"ddMMyyyy\").format($F{EXPIRE_DATE})+\".\"+\n" +
            "new java.text.DecimalFormat(\"###0\").format($F{DOC_AMOUNT})+\n" +
            "new java.text.DecimalFormat(\"###0\").format($F{FINE_AMOUNT})+\n" +
            "new java.text.DecimalFormat(\"###0\").format($F{PAY_VALUE})]]></jr:codeExpression>\n" +
            "\t\t\t\t</jr:barbecue>\n" +
            "\t\t\t</componentElement>\n" +
            "\t\t\t<textField>\n" +
            "\t\t\t\t<reportElement uuid=\"9851bccc-11db-41a0-9cc9-50678f07338a\" x=\"278\" y=\"83\" width=\"267\" height=\"13\"/>\n" +
            "\t\t\t\t<textElement textAlignment=\"Right\" verticalAlignment=\"Middle\">\n" +
            "\t\t\t\t\t<font isBold=\"true\"/>\n" +
            "\t\t\t\t</textElement>\n" +
            "\t\t\t\t<textFieldExpression><![CDATA[$F{NF_ID}+\".\"+\n" +
            "$F{CNPJ}+\".\"+\n" +
            "new SimpleDateFormat(\"ddMMyyyy\").format($F{DOC_DATE})+\n" +
            "new SimpleDateFormat(\"ddMMyyyy\").format($F{EXPIRE_DATE})+\".\"+\n" +
            "new java.text.DecimalFormat(\"###0\").format($F{DOC_AMOUNT})+\n" +
            "new java.text.DecimalFormat(\"###0\").format($F{FINE_AMOUNT})+\n" +
            "new java.text.DecimalFormat(\"###0\").format($F{PAY_VALUE})]]></textFieldExpression>\n" +
            "\t\t\t</textField>\n" +
            "\t\t</band>\n" +
            "\t</detail>\n" +
            "</jasperReport>\n";

    public void handleToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Toggled", "Visibility:" + event.getVisibility());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }


}