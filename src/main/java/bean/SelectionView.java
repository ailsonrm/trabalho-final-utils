package bean;

import entity.NF;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

@ManagedBean (name = "selection")
@RequestScoped
public class SelectionView implements Serializable {

    //ATTRIBUTES
    @ManagedProperty("#{nfService}")
    private NFService service;
    private List<NF> nfs;
    private NF selectedNF;


    //METHODS
    @PostConstruct
    void initialiseSession ()  {
        FacesContext. getCurrentInstance(). getExternalContext (). getSession ( true );
    }

    @PostConstruct
    public void init() {
        nfs = service.createNF();
    }

    public NF getSelectedNF() {
        return selectedNF;
    }

    public void setSelectedNF(NF selectedNF) {
        this.selectedNF = selectedNF;
    }

    public void setService(NFService service) {
        this.service = service;
    }

    public List<NF> getNfs() {
        return nfs;
    }
}
