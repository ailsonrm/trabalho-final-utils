package bean;

import dao.NFDAO;
import entity.NF;
import factory.DAOFactory;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.SimpleFormatter;

@ManagedBean(name = "nfService")
@RequestScoped
public class NFService {

    //METHODS
    public List<NF> createNF() {
        List<NF> nfs = new ArrayList<NF>();
        NFDAO nfdao = DAOFactory.getDaoFactory(DAOFactory.NF_CLASS).getNFDAO();
        nfs = nfdao.getALLNFs();
        return nfs;
    }
}
