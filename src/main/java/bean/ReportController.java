package bean;

import entity.NF;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.bind.SchemaOutputResolver;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@ManagedBean (name = "reportController")
@SessionScoped
public class ReportController {

    public StreamedContent getExportReportPDF(NF pNF) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {

        String userName = "root";
        String password = "fiap";
        String driver = "com.mysql.jdbc.Driver";
        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String nomeDoArquivo = "Dados de Nota fiscal não inexistentes";
        Map<String, Object> params = new HashMap<String, Object>();

        if (pNF != null) {
            nomeDoArquivo = (pNF.getId()+"-"+pNF.getNome());
            params.put("NF_ID", BigDecimal.valueOf(pNF.getId()));
            params.put("COM_BOLETO", pNF.isBilet());
        }

        try {
            Class.forName(driver).newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/trab_final_utils", userName, password);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(getClass().getClassLoader().getResourceAsStream("NF_Info.jasper"));
            JasperPrint print = JasperFillManager.fillReport(jasperReport, params, conn);

            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, baos);
            exporter.exportReport();
            inputStream = new ByteArrayInputStream(baos.toByteArray());
            conn.close();
        } catch (JRException jrException) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, jrException);
        }
        return new DefaultStreamedContent(inputStream,"application/pdf; charset=UTF-8",("NF_"+nomeDoArquivo+".pdf"));
    }
}
