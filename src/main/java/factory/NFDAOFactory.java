package factory;

import dao.NFDAO;

public class NFDAOFactory extends DAOFactory {

    @Override
    public NFDAO getNFDAO() {
        return new NFDAO();
    }
}
