package factory;

import dao.NFDAO;

public abstract class DAOFactory {

    public static final String NF_CLASS = "NF_CLASS";

    public abstract NFDAO getNFDAO();

    public static DAOFactory getDaoFactory(String pDAOType) {
        switch (pDAOType) {
            case NF_CLASS:
                return new NFDAOFactory();
            default:
                return null;
        }
    }
}
