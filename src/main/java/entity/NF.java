package entity;

import bean.NFService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "NF")
@ManagedBean(name = "nf")
public class NF implements Serializable {


    //ATTRIBUTES //teste
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "NF_ID")
    private int id;

    @Column(name = "NAME")
    private String nome;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "CITY")
    private String city;

    @Column(name = "UF")
    private String uf;

    @Column(name = "CNPJ")
    private String cnpj;

    @Column(name = "BANK_NAME")
    private String bankName;

    @Column(name = "EXPIRE_DATE")
    private Date expirationDate;

    @Column(name = "DOC_DATE")
    private Date documentDate;

    @Column(name = "OBSERVATION")
    private String observation;

    @Column(name = "DOC_AMOUNT")
    private double documentAmount;

    @Column(name = "FINE_AMOUNT")
    private double fineAmount;

    @Column(name = "PAY_VALUE")
    private double paymentValue;

    @Column(name = "BILET")
    public boolean bilet;

    public static NFService nfService = new NFService();

    //CONSTRUCTORS
    public NF() {
    }

    public NF(String nome, String address, String city, String uf, String cnpj, String bankName, Date expirationDate, Date documentDate, String observation, double documentAmount, double fineAmount, double paymentValue, boolean bilet) {
        this.nome = nome;
        this.address = address;
        this.city = city;
        this.uf = uf;
        this.cnpj = cnpj;
        this.bankName = bankName;
        this.expirationDate = expirationDate;
        this.documentDate = documentDate;
        this.observation = observation;
        this.documentAmount = documentAmount;
        this.fineAmount = fineAmount;
        this.paymentValue = paymentValue;
        this.bilet = bilet;
    }

    //METHODS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public double getDocumentAmount() {
        return documentAmount;
    }

    public void setDocumentAmount(double documentAmount) {
        this.documentAmount = documentAmount;
    }

    public double getFineAmount() {
        return fineAmount;
    }

    public void setFineAmount(double fineAmount) {
        this.fineAmount = fineAmount;
    }

    public double getPaymentValue() {
        return paymentValue;
    }

    public void setPaymentValue(double paymentValue) {
        this.paymentValue = paymentValue;
    }

    public boolean isBilet() {
        return bilet;
    }

    public void setBilet(boolean bilet) {
        this.bilet = bilet;
    }
}
