package repository;

import dao.NFDAO;
import factory.DAOFactory;

public class DAORepository {

    static NFDAO nfdao;

    public static NFDAO getNFDAO() {

        if (nfdao == null) {
            nfdao = DAOFactory.getDaoFactory(DAOFactory.NF_CLASS).getNFDAO();
        }
        return nfdao;
    }
}
